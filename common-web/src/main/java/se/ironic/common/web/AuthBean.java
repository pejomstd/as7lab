/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.common.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Peter
 */
@Named
@SessionScoped
public class AuthBean implements Serializable {

    private AuthUser user;

    public void logout() {
        try {
            FacesContext currentFC = FacesContext.getCurrentInstance();
            ExternalContext ectx = currentFC.getExternalContext();
            ectx.invalidateSession();
            ectx.redirect(ectx.getRequestContextPath() + "/common/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(AuthBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean loggedIn() {
        FacesContext currentFC = FacesContext.getCurrentInstance();
        ExternalContext ectx = currentFC.getExternalContext();
        return ectx.getRemoteUser() != null;
    }

    public String getLoggedInUser() {
        FacesContext currentFC = FacesContext.getCurrentInstance();
        ExternalContext ectx = currentFC.getExternalContext();
        return ectx.getRemoteUser();
    }
}
