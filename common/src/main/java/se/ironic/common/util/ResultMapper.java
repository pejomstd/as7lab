package se.ironic.common.util;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Peter
 */
public class ResultMapper {
    public static <T> List<T> map(List<Object[]> resultList, List<String> fields, Class<T> resultClass) throws Exception {
        String[] toArray = fields.toArray(new String[]{});
        return map(resultList, fields, resultClass);
    }
    
    public static <T> List<T> map(List<Object[]> resultList, String[] fields, Class<T> resultClass) throws Exception {
        List<T> mapped = new ArrayList<T>();
        for(Object[] oa : resultList) {
            mapped.add(map(oa, fields, resultClass));
        }
        return mapped;
    }
    
    public static <T> T map(Object[] resultRow, String[] fields, Class<T> resultClass) throws Exception {
        if(resultRow.length != fields.length) {
            throw  new IllegalArgumentException("Length of resultRow must match the number of fields");
        }
        
        T t = resultClass.newInstance();
        for(int i = 0; i < fields.length; i++) {
            Field f = t.getClass().getDeclaredField(fields[i]);
            f.setAccessible(true);
            f.set(t, resultRow[i]);
        }
        return t;
    }
    
    public static <T> T map(Object[] resultRow, List<String> fields, Class<T> resultClass) throws Exception {
        String[] toArray = fields.toArray(new String[]{});
        return map(resultRow, toArray, resultClass);
    }
}
