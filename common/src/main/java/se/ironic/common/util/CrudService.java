/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.common.util;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;


public abstract class CrudService {

    public abstract EntityManager getEntityManager();
    
    public <T> T create(T t) {
        getEntityManager().persist(t);
        return t;
    }

    public <T> T update(T t) {
        return getEntityManager().merge(t);
    }

    public <T> void delete(T t) {
        T m = getEntityManager().merge(t);
        getEntityManager().remove(m);
    }

    public <T> T find(Object id, Class<T> type) {
        return getEntityManager().find(type, id);
    }

    public <T> List<T> findByNamedQuery(String namedQuery) {
        return getEntityManager().createNamedQuery(namedQuery).getResultList();
    }

    public <T> List<T> findByNamedQuery(String namedQuery, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> entrySet = parameters.entrySet();
        Query q = getEntityManager().createNamedQuery(namedQuery);
        for (Map.Entry<String, Object> entry : entrySet) {
            q.setParameter(entry.getKey(), entry.getValue());
        }
        return q.getResultList();
    }

    public <T> List<T> findByNativeQuery(String nativeQuery, List<String> fields, Class<T> resultClass) {
        String[] toArray = fields.toArray(new String[]{});
        return findByNativeQuery(nativeQuery, toArray, resultClass);
    }

    public <T> List<T> findByNativeQuery(String nativeQuery, Map<String, Object> parameters, List<String> fields, Class<T> resultClass) {
        String[] toArray = fields.toArray(new String[]{});
        return findByNativeQuery(nativeQuery, parameters, toArray, resultClass);
    }

    public <T> List<T> findByNativeQuery(String nativeQuery, String[] fields, Class<T> resultClass) {
        Query q = getEntityManager().createNativeQuery(nativeQuery);
        try {
            // Hibernate JPA implementation is a bit unwilling to take non @Entity classes as type to createNativeQuery()
            List mapped = ResultMapper.map(q.getResultList(), fields, resultClass);
            return mapped;
        } catch (Exception ex) {
            Logger.getLogger(CrudService.class.getName()).log(Level.WARNING,
                    "Could not map result from native query", ex);
        }
        return null;
    }

    public <T> List<T> findByNativeQuery(String nativeQuery, Map<String, Object> parameters, String[] fields, Class<T> resultClass) {
        Set<Map.Entry<String, Object>> entrySet = parameters.entrySet();
        Query q = getEntityManager().createNamedQuery(nativeQuery);
        for (Map.Entry<String, Object> entry : entrySet) {
            q.setParameter(entry.getKey(), entry.getValue());
        }
        try {
            // Hibernate JPA implementation is a bit unwilling to take non @Entity classes as type to createNativeQuery()
            List mapped = ResultMapper.map(q.getResultList(), fields, resultClass);
            return mapped;
        } catch (Exception ex) {
            Logger.getLogger(CrudService.class.getName()).log(Level.WARNING, 
                    "Could not map result from native query", ex);
        }
        return null;
    }
    
    
}
