/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.common.util;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.naming.InitialContext;
import org.apache.deltaspike.core.api.provider.BeanProvider;

/**
 *
 * @author Peter
 */
public class InjectionUtil {

    public static Object getContextualReference(Class<?> clazz, AnnotationLiteral<?> literal) {
        return BeanProvider.getContextualReference(clazz, false, literal);
    }

    public static Object inject(Class<?> clazz, AnnotationLiteral<?> literal) {
        try {
            BeanManager bm = getBeanManager();
            Set<Bean<?>> beans = bm.getBeans(CrudService.class, literal);
            // should be only one
            if (beans.iterator().hasNext()) {
                Bean<?> bean = beans.iterator().next();
                CreationalContext<? extends Object> createCreationalContext = bm.createCreationalContext(bean);
                Object reference = bm.getReference(bean, CrudService.class, createCreationalContext);
                return reference;
            }
        } catch (Exception ex) {
            Logger.getLogger(InjectionUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static BeanManager getBeanManager() throws Exception {
        InitialContext ctx = new InitialContext();
        BeanManager lookup = (BeanManager) ctx.lookup("java:comp/BeanManager");
        return lookup;
    }
}
