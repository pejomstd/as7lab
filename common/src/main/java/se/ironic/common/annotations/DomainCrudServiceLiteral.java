/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.common.annotations;

import javax.enterprise.util.AnnotationLiteral;

/**
 *
 * @author Peter
 */
public class DomainCrudServiceLiteral extends AnnotationLiteral<DomainCrudService> implements DomainCrudService {
    private String domain;
    public DomainCrudServiceLiteral(String domain) {
        this.domain = domain;
    }
    @Override
    public String domain() {
        return domain;
    }
    
}
