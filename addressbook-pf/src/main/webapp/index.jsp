<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"  
      xmlns:h="http://java.sun.com/jsf/html"  
      xmlns:f="http://java.sun.com/jsf/core"  
      xmlns:p="http://primefaces.org/ui">  

    <h:head>  

    </h:head>  

    <h:body>  

        <h:form>  

            <h:panelGrid columns="4" cellpadding="5">  
                <h:outputLabel for="name" value="Name:" style="font-weight:bold"/>  

                <p:inputText id="name" value="#{contactBean.firstname}" />  

                <p:commandButton value="Submit" update="display"/>  

                <h:outputText value="#{contactBean.firstname}" id="display" />  
            </h:panelGrid>  

        </h:form>  

    </h:body>  
</html>     