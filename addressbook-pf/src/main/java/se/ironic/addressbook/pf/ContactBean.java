/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.pf;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Peter
 */
@ManagedBean(name = "contact")
@RequestScoped
public class ContactBean implements Serializable {
    private String firstname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
}
