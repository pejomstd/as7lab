/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.pf;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import se.ironic.addressbook.domain.Contact;
import se.ironic.addressbook.service.AdressbookService;

/**
 *
 * @author Peter
 */
@ManagedBean
@RequestScoped
public class ContactController {
    @Inject AdressbookService addressbookService;
    public List<Contact> findAll() {
        return addressbookService.findAllContacts();
    }
    
    
}
