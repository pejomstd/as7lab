/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.client;

import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import se.ironic.addressbook.service.DAOServiceRemote;
//import org.jboss.ejb.client.ContextSelector;
//import org.jboss.ejb.client.EJBClientConfiguration;
//import org.jboss.ejb.client.EJBClientContext;
//import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
//import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;
import se.ironic.addressbook.service.PingService;
import se.ironic.addressbook.domain.Contact;

/**
 *
 * @author Peter
 */
public class Client {

    public static void main(String[] args) {
        try {
//            Properties clientProp = new Properties();
//
//            clientProp.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
//            clientProp.put("remote.connections", "default");
//            clientProp.put("remote.connection.default.port", "4447");
//            clientProp.put("remote.connection.default.host", "localhost");
//            clientProp.put("remote.connection.default.username", "appUser");
//            clientProp.put("remote.connection.default.password", "appPwd");
//            clientProp.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");
//
//            EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(clientProp);
//            ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(cc);
//            EJBClientContext.setSelector(selector);
//
//            Properties props = new Properties();
//            props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
//            Context ctx = new InitialContext(props);

            Properties p = new Properties();
            p.load(Client.class.getResourceAsStream("/jndi.properties"));
            Context ctx = new InitialContext(p);
            PingService ps = (PingService) ctx.lookup("ejb:AddressbookDAO-ear/AddressbookDAO/PingBean!se.ironic.addressbook.service.PingService");
            System.out.println("Ping : " + ps.ping());
            System.out.println("Pong : " + ps.pong());
            DAOServiceRemote lookup = (DAOServiceRemote) ctx.lookup("ejb:AddressbookDAO-ear/AddressbookDAO/DAOServiceImpl!" + DAOServiceRemote.class.getName());
            List<Contact> findAll = lookup.findByNamedQuery(Contact.ALL);
            for (Contact contact : findAll) {
                System.out.println("Contact: " + contact.getFirstname() + " " + contact.getLastname());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
