/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import se.ironic.addressbook.domain.Contact;
import se.ironic.common.annotations.DomainCrudService;
import se.ironic.common.util.CrudService;

/**
 *
 * @author Peter
 */
@Stateless
@LocalBean
//@RunAs("appRole")
@Path(value = "/addressbook")
public class AdressbookService {

   
    @Inject @DomainCrudService(domain = "addressbook")
    CrudService crudService;
    
    @GET
    @Path(value = "xml")
    @Produces({"application/xml"})
    public ContactList getXmlContactList() {
        ContactList cList = new ContactList();
        cList.setContacts(findAllContacts());
        
        return cList;
    }
    
    @GET
    @Path(value = "json")
    @Produces({"application/json"})
    public ContactList getJsonContactList() {
        ContactList cList = new ContactList();
        cList.setContacts(findAllContacts());
        return cList;
    }
    
    public List<Contact> findAllContacts() {
        return crudService.findByNamedQuery(Contact.ALL);
    }
    
    public Contact findContactById(Long id) {
        return crudService.find(id, Contact.class);
    }
    
    public void saveContact(Contact c) {
        crudService.create(c);
    }
}
