/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Peter
 */
@Stateless
@LocalBean
@DeclareRoles({"appRole"})
public class SecureService {
    
    @RolesAllowed({"appRole"})
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
