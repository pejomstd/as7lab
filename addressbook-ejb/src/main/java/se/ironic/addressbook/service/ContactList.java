/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import se.ironic.addressbook.domain.Contact;

/**
 *
 * @author Peter
 */
@XmlRootElement
public class ContactList {
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }
    
}
