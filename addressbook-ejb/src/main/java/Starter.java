
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import se.ironic.common.annotations.DomainCrudServiceLiteral;
import se.ironic.common.util.CrudService;
import se.ironic.common.util.InjectionUtil;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Peter
 */
@Singleton
@Startup
public class Starter {

    @PostConstruct
    public void setUp() {
//        Object inject = InjectionUtil.getContextualReference(CrudService.class, new DomainCrudServiceLiteral("addressbook"));
        Object inject = InjectionUtil.inject(CrudService.class, new DomainCrudServiceLiteral("addressbook"));
        System.out.println("\n\nInjected: " + String.valueOf(inject));
    }
}
