/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import java.util.Properties;
import javax.annotation.ManagedBean;
import javax.ejb.embeddable.EJBContainer;
import javax.inject.Inject;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.InitialContext;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Peter
 */
@ManagedBean
public class SecureServiceTest  {

    @Inject
    SecureService secureService;

    public SecureServiceTest() {
    }

    @Before
    public void setUp() throws Exception {
        Properties p = new Properties();
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        EJBContainer.createEJBContainer(p).getContext().bind("inject", this);
    }

    @Test
    public void testSayHelloUnsecure() throws Exception {
        System.out.println("testSayHelloUnsecure()");
        try {
            // User does not exist
            setUser("testUnauth", "testPwd");
            String sayHello = secureService.sayHello("Test");
            fail("Should have gotten AuthenticationException");
        } catch (AuthenticationException ex) {
            System.out.println("Login attempt failed as expected");
        }
        System.out.println("// end testSayHelloUnsecure()");
    }

    @Test
    public void testSayHelloSecure() throws Exception {
        System.out.println("testSayHelloSecure()");
        setUser("testUser", "testPwd");
        String sayHello = secureService.sayHello("Test");
        System.out.println("// end testSayHelloSecure()");
    }

    private void setUser(String user, String pwd) throws Exception {
        Properties login = new Properties();
        login.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        login.put(Context.SECURITY_PRINCIPAL, user);
        login.put(Context.SECURITY_CREDENTIALS, pwd);
        InitialContext ctx = new InitialContext(login);

    }
}