/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Setup to make rest available under path /rest
 * @author Peter
 */
@ApplicationPath("rest")
public class ServiceApplication extends Application {
    
}
