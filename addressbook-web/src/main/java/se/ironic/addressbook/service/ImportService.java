/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import java.security.Principal;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 * REST WS to import some kind of XML passed in a String
 * @author Peter
 */
@Path("/import")
public class ImportService {
    
    /**
     * Method for handling import of a XML string
     * @param context Security context for auth.
     * @param headers httpHeaders to make it possible to send extra parameters in http headers
     * @param xml the content to import
     */
    @POST
    public Response importXml(@Context SecurityContext context, @Context HttpHeaders headers, String xml) {
        String user = getUsername(context);
        System.out.println("XML: " + xml);
        MultivaluedMap<String, String> requestHeaders = headers.getRequestHeaders();
        for (String key : requestHeaders.keySet()) {
           for(String val : requestHeaders.get(key)) {
               System.out.println("Header: " + key + ", value: " + val);
           } 
        }
        return Response.status(Status.OK).build();
    }
    
    @GET
    @Path("xml/{name}")
    public String getXml(@PathParam("name") String name) {
        return "<root><name>"+name+"</name></root>";
    }
    
    private String getUsername(SecurityContext context) {
        Principal principal = null;

        if (context != null)
            principal = context.getUserPrincipal();

        if (principal == null)
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);

        return principal.getName();
    }
}
