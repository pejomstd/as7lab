/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.addressbook.view;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter
 */
public class StaffMember {
    private String name;
    private String function;
    private List<Skill> skills = new ArrayList<Skill>();

    public StaffMember(String name, String function) {
        this.name = name;
        this.function = function;
    }

    public StaffMember() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
    
    
}
