/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import se.ironic.addressbook.domain.Contact;
import se.ironic.addressbook.domain.Phone;

/**
 *
 * @author Peter
 */
@Named(value = "contactBean")
@RequestScoped
public class ContactBean {

    private Contact contact = new Contact();

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Phone> getPhones() {
        List<Phone> pList = new ArrayList<Phone>();
        pList.addAll(getContact().getPhones());
        return pList;
    }
}
