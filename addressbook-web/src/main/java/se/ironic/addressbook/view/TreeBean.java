/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
//import org.richfaces.model.TreeNode;

/**
 *
 * @author Peter
 */
// TODO kan inject.ManagedBean användas?
@ManagedBean(name = "treeBean")
@ViewScoped
public class TreeBean implements Serializable {

    private List<TreeNode> rootNodes = new ArrayList<TreeNode>();
    TreeNode currentSelection;

    @PostConstruct
    public void init() {
        GenTreeNode<Org> root = new GenTreeNode<Org>();
        Org buildOrgTree = buildOrgTree();
        orgToTreeNodes(buildOrgTree, root);
        rootNodes.add(root);
    }

    public List<TreeNode> getRootNodes() {
        return rootNodes;
    }

    public void setRootNodes(List<TreeNode> rootNodes) {
        this.rootNodes = rootNodes;
    }

    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
        System.out.println("selectionChangeEvent");
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();

        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        currentSelection = (TreeNode) tree.getRowData();
        tree.setRowKey(storedKey);
    }

    private void orgToTreeNodes(Org org, GenTreeNode<Org> node) {
        node.setData(org);
        if (!org.getChildren().isEmpty()) {
            for (Org oc : org.getChildren()) {
                GenTreeNode<Org> newTreeNode = new GenTreeNode<Org>();
                node.addChild(newTreeNode);
                newTreeNode.setParent(node);
                orgToTreeNodes(oc, newTreeNode);
            }
        }
    }

    public Org buildOrgTree() {
        Org org = new Org(1, "Company head");
        org.getChildren().add(new Org(2, "HQ"));
        org.getChildren().add(new Org(3, "Sales"));
        Org it = new Org(4, "IT");
        org.getChildren().add(it);
        it.getChildren().add(new Org(4, "R&D"));
        return org;
    }

    public TreeNode getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(TreeNode currentSelection) {
        this.currentSelection = currentSelection;
    }

}
