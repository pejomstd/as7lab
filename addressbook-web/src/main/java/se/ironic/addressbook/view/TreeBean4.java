/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AbortProcessingException;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.event.TreeSelectionChangeListener;
import org.richfaces.model.TreeNode;

/**
 *
 * @author Peter
 */
// TODO kan inject.ManagedBean användas?
@ManagedBean(name = "treeBean4")
@ViewScoped
public class TreeBean4 implements TreeSelectionChangeListener, Serializable {

    private List<TreeNode> rootNodes = new ArrayList<TreeNode>();
    TreeNode currentSelection;
    DataTreeNode<Org> root;

    @PostConstruct
    public void init() {
        root = new DataTreeNode<Org>();
        Org buildOrgTree = buildOrgTree();
        DataTreeNode<Org> first = new DataTreeNode<Org>();
        root.addChild(buildOrgTree.getId(), first);
        orgToTreeNodes(buildOrgTree, first);
        rootNodes.add(root);
    }

    public List<TreeNode> getRootNodes() {
        return rootNodes;
    }

    public void setRootNodes(List<TreeNode> rootNodes) {
        this.rootNodes = rootNodes;
    }

    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
        System.out.println("selectionChangeEvent");
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();

        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        currentSelection = (DataTreeNode<Org>) tree.getRowData();
        tree.setRowKey(storedKey);
    }

    private void orgToTreeNodes(Org org, DataTreeNode<Org> node) {
        node.setData(org);
        if (!org.getChildren().isEmpty()) {
            for (Org oc : org.getChildren()) {
                DataTreeNode<Org> newTreeNode = new DataTreeNode<Org>();
                node.addChild(String.valueOf(oc.getId()), newTreeNode);
                orgToTreeNodes(oc, newTreeNode);
            }
        }
    }

    public Org buildOrgTree() {
        Org org = new Org(1, "Company head");
        org.getChildren().add(new Org(2, "HQ"));
        org.getChildren().add(new Org(3, "Sales"));
        Org it = new Org(4, "IT");
        org.getChildren().add(it);
        it.getChildren().add(new Org(4, "R&D"));
        return org;
    }

    public TreeNode getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(TreeNode currentSelection) {
        this.currentSelection = currentSelection;
    }

    public DataTreeNode<Org> getRoot() {
        return root;
    }

    public void setRoot(DataTreeNode<Org> root) {
        this.root = root;
    }

    @Override
    public void processTreeSelectionChange(TreeSelectionChangeEvent tsce) throws AbortProcessingException {
        System.out.println("processTreeSelectionChange");
    }

    
}
