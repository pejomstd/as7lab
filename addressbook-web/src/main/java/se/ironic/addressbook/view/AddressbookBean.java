/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import se.ironic.addressbook.service.AdressbookService;
import se.ironic.addressbook.domain.Contact;

/**
 *
 * @author Peter
 */
@Named(value = "addressbookBean")
@SessionScoped
public class AddressbookBean implements Serializable {

    @EJB
    AdressbookService addressbookService;
    private List<Contact> contacts;

    /**
     * Creates a new instance of AddressbookBean
     */
    public AddressbookBean() {
    }

    @PostConstruct
    private void init() {
        contacts = addressbookService.findAllContacts();
    }

    public String refresh() {
        contacts = addressbookService.findAllContacts();
        return "addressbook.xhtml";
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName) {
        FacesContext context = FacesContext.getCurrentInstance();
        return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
    }

    public String loadContact() {
        FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        ContactBean contactBean = (ContactBean) findBean("contactBean");
        
        Long id = 31L;
        Contact c = addressbookService.findContactById(id);
        contactBean.setContact(c);
        return "contact.xhtml";
    }
}
