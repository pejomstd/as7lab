/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.addressbook.view;

import com.google.common.collect.Iterators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.tree.TreeNode;
//import org.richfaces.model.TreeNode;

/**
 *
 * @author Peter
 * @param <T>
 */
public class GenTreeNode<T> implements TreeNode, Serializable {
    private T data;
    private GenTreeNode<T> parent;
    private List<GenTreeNode<T>> childs = new ArrayList<GenTreeNode<T>>();
    
    public T getData() {
        return data;
    }

    public void addChild(GenTreeNode<T> child) {
        childs.add(child);
    }
    
    public void setData(T data) {
        this.data = data;
    }
    
    @Override
    public TreeNode getChildAt(int i) {
        return childs.get(i);
    }

    @Override
    public int getChildCount() {
        return childs.size();
    }

    @Override
    public TreeNode getParent() {
        return parent;
    }

    public void setParent(GenTreeNode<T> parent) {
        this.parent = parent;
    }

    @Override
    public int getIndex(TreeNode tn) {
        return childs.indexOf(tn);
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    @Override
    public boolean isLeaf() {
        return childs.isEmpty();
    }

    @Override
    public Enumeration children() {
        return Iterators.asEnumeration(childs.iterator());
    }

    
    
    
}
