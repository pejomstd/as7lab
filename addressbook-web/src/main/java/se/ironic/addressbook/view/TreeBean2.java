/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
//import org.richfaces.model.TreeNode;

/**
 *
 * @author Peter
 */
// TODO kan inject.ManagedBean användas?
@ManagedBean(name = "treeBean2")
@ViewScoped
public class TreeBean2 implements Serializable {

    private List<Org> rootNodes = new ArrayList<Org>();
    Org currentSelection;

    @PostConstruct
    public void init() {
        Org buildOrgTree = buildOrgTree();
        rootNodes.add(buildOrgTree);
    }

    public List<Org> getRootNodes() {
        return rootNodes;
    }

    public void setRootNodes(List<Org> rootNodes) {
        this.rootNodes = rootNodes;
    }

    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
        System.out.println("selectionChangeEvent");
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();

        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        currentSelection = (Org) tree.getRowData();
        tree.setRowKey(storedKey);
    }

    public Org buildOrgTree() {
        Org org = new Org(1, "Company head");
        org.getChildren().add(new Org(2, "HQ"));
        org.getChildren().add(new Org(3, "Sales"));
        Org it = new Org(4, "IT");
        org.getChildren().add(it);
        it.getChildren().add(new Org(4, "R&D"));
        return org;
    }

    public Org getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(Org currentSelection) {
        this.currentSelection = currentSelection;
    }

}
