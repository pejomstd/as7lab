/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
//import org.richfaces.model.TreeNode;

/**
 *
 * @author Peter
 */
// TODO kan inject.ManagedBean användas?
@ManagedBean(name = "treeBean3")
@ViewScoped
public class TreeBean3 implements Serializable {

    private List<Org> rootNodes = new ArrayList<Org>();
    Org currentOrg;
    StaffMember currentStaff;

    @PostConstruct
    public void init() {
        /* Does not work exactly with rich:treeModelAdaptor */
        Org org = new Org(1, "Company root");
        Org it = new Org(2, "IT-department");
        org.getChildren().add(it);
        StaffMember member = new StaffMember("John", "Programmer");
        member.getSkills().add(new Skill("Java"));
        StaffMember cio = new StaffMember("Jane", "CIO");
        org.getStaff().add(cio);
        it.getStaff().add(member);
        rootNodes.add(org);
    }

    public List<Org> getRootNodes() {
        return rootNodes;
    }

    public void setRootNodes(List<Org> rootNodes) {
        this.rootNodes = rootNodes;
    }

    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();

        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        if (tree.getRowData() instanceof Org) {
            currentOrg = (Org) tree.getRowData();
            currentStaff = null;
        } 
        if (tree.getRowData() instanceof StaffMember) {
            currentStaff = (StaffMember) tree.getRowData();
            currentOrg = null;
        } 
        tree.setRowKey(storedKey);
    }

    public Org buildOrgTree() {
        Org org = new Org(1, "Company head");
        org.getChildren().add(new Org(2, "HQ"));
        org.getChildren().add(new Org(3, "Sales"));
        Org it = new Org(4, "IT");
        org.getChildren().add(it);
        it.getChildren().add(new Org(4, "R&D"));
        return org;
    }

    public Org getCurrentOrg() {
        return currentOrg;
    }

    public void setCurrentOrg(Org currentOrg) {
        this.currentOrg = currentOrg;
    }

    public StaffMember getCurrentStaff() {
        return currentStaff;
    }

    public void setCurrentStaff(StaffMember currentStaff) {
        this.currentStaff = currentStaff;
    }

    

}
