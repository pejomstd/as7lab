/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.addressbook.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.richfaces.model.TreeNodeImpl;

/**
 *
 * @author Peter
 */
public class DataTreeNode<T> extends TreeNodeImpl implements Serializable {
    private T data;
    
    public DataTreeNode() {
        super();
    }

    public DataTreeNode(T data, boolean leaf) {
        super(leaf);
        this.data = data;
    }
    
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return String.valueOf(data);
    }

}
