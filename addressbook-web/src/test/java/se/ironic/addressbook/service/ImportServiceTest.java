/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.service;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientRequestFactory;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClientExecutor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Peter
 */
public class ImportServiceTest {
    
    public ImportServiceTest() {
    }

    /**
     * Test of importXml method, of class ImportService without authorization
     */
    @Test
    public void testImportXmlUnAuth() {
        try {
            System.out.println("importXmlUnAuth");
            String xml = "<test>TEST</test>";
            
            ClientRequest cr = new ClientRequest("http://localhost:8080/addressbook-web/rest/import/");
            cr.body("text/plain", xml);
            ClientResponse postResponse = cr.post();
            assertEquals("Should get an Auth failure response", 401, postResponse.getStatus());
        } catch (Exception ex) {
            Logger.getLogger(ImportServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /**
     * Test of importXml method, of class ImportService with authorization
     */
    @Test
    public void testImportXmlAuth() {
        try {
            System.out.println("importXmlAuth");
            String xml = "<test>TEST-auth</test>";
            
            HttpClient hc = new HttpClient();
            hc.getState().setCredentials(
                    AuthScope.ANY, 
                    new UsernamePasswordCredentials("testuser","testuser1"));
            
            
            ClientRequestFactory crFact = new ClientRequestFactory(
                    new ApacheHttpClientExecutor(hc), 
                    new URI("http://localhost:8080/addressbook-web/rest/import/"));
            
            ClientRequest cr = crFact.createRequest("http://localhost:8080/addressbook-web/rest/import/");
            cr.body("text/plain", xml);
            cr.header("to", "someSystem");
            cr.header("from", "someOtherSystem");
            ClientResponse postResponse = cr.post();
            assertEquals("Should get an OK ", 200, postResponse.getStatus());
        } catch (Exception ex) {
            Logger.getLogger(ImportServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
}