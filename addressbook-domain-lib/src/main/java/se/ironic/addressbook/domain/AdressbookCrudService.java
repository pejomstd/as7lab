/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import se.ironic.common.util.CrudService;
import se.ironic.common.annotations.DomainCrudService;

/**
 *
 * @author Peter
 */
@DomainCrudService(domain = "addressbook")
public class AdressbookCrudService extends CrudService {
    @PersistenceContext(unitName =  "addressbookPU")
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public static CrudService create() {
        AdressbookCrudService cs = new AdressbookCrudService();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("addressbookPU");
        cs.em = emf.createEntityManager();
        return cs;
    } 
}
