/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.ironic.common.annotations.DomainCrudService;
import se.ironic.common.util.CrudService;

/**
 *
 * @author Peter
 */
@DomainCrudService(domain = "another")
public class AnotherCService extends CrudService {
    @PersistenceContext(unitName =  "addressbookPU")
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
