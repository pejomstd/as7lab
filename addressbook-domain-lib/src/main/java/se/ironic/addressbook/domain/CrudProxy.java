/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import javax.ejb.Stateless;
import javax.inject.Inject;
import se.ironic.common.annotations.DomainCrudService;
import se.ironic.common.util.CrudService;

/**
 *
 * @author Peter
 */
@Stateless
public class CrudProxy {
    @Inject @DomainCrudService(domain = "addressbook")
    protected CrudService crudService;

    public CrudService getCrudService() {
        return crudService;
    }
    
    
}
