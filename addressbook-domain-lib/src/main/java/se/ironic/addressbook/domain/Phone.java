/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Peter
 */
@Embeddable
public class Phone implements Serializable {
    private String countryCode;
    private String areaCode;
    private String number;
    private PhoneType type;

    public Phone() {
    }

    public Phone(String countryCode, String areaCode, String number, PhoneType type) {
        this.countryCode = countryCode;
        this.areaCode = areaCode;
        this.number = number;
        this.type = type;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }
    
}
