/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Peter
 */
@XmlRootElement
@Entity
@NamedQueries({
    @NamedQuery(name = Contact.ALL, query = "SELECT c FROM Contact c"),
    @NamedQuery(name = Contact.BY_FIRSTNAME, query = "SELECT c FROM Contact c WHERE c.firstname = :firstname"),
    @NamedQuery(name = Contact.BY_LASTNAME, query = "SELECT c FROM Contact c WHERE c.lastname = :lastname")
})
public class Contact extends AbstractEntity {
    public static final String ALL = "Contact.findAll";
    public static final String BY_FIRSTNAME = "Contact.findByFirstname";
    public static final String BY_LASTNAME = "Contact.findByLastname";
    private static final long serialVersionUID = 1L;
//    @Id
//    @GeneratedValue //(strategy = GenerationType.AUTO)
//    private Long id;
    private String firstname;
    private String lastname;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "contact_id")
    private List<Address> addresses = new ArrayList<Address>();
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Phone> phones = new HashSet<Phone>();

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "se.ironic.addressbook.vo.Contact[ id=" + getId() + " ]";
    }

    void setId(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
