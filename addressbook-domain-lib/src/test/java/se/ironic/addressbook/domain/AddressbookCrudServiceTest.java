/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.addressbook.domain;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.ejb.embeddable.EJBContainer;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.naming.Context;
import javax.transaction.UserTransaction;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import javax.inject.Inject;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import se.ironic.common.annotations.DomainCrudService;
import se.ironic.common.annotations.DomainCrudServiceLiteral;
import se.ironic.common.util.CrudService;
import se.ironic.common.util.InjectionUtil;

/**
 *
 * @author Peter
 */
//@LocalClient
@ManagedBean
public class AddressbookCrudServiceTest {

    @Inject
    Logger logger;
    @Inject
    BeanManager beanManager;
    @Inject
    @DomainCrudService(domain = "addressbook")
    protected CrudService crudService;
    @Resource
    private UserTransaction userTransaction;

    public AddressbookCrudServiceTest() {
    }

    @Before
    public void setUp() throws Exception {
        Properties p = new Properties();
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        p.put("jdbc/Addressbook", "new://Resource?type=DataSource");
        p.put("jdbc/Addressbook.JdbcDriver", "org.postgresql.Driver");
        p.put("jdbc/Addressbook.UserName", "dev");
        p.put("jdbc/Addressbook.Password", "dev");
        p.put("jdbc/Addressbook.JdbcUrl", "jdbc:postgresql://localhost:5432/addressbook_test");
//        p.put(Context.SECURITY_PRINCIPAL, "testUser");
//        p.put(Context.SECURITY_CREDENTIALS, "testPwd");
        EJBContainer.createEJBContainer(p).getContext().bind("inject", this);

    }

    @Test
    public void testStaticInject() throws Exception {
//        CrudService inject = BeanProvider.getContextualReference(
//                CrudService.class, false, new DomainCrudServiceLiteral("addressbook"));
        Object inject = InjectionUtil.inject(CrudService.class, new DomainCrudServiceLiteral("addressbook"));
        System.out.println("Injected: " + inject);
    }

    @Test
    public void testFindAllMethod() {
        try {
            List<Contact> findAll = crudService.findByNamedQuery(Contact.ALL);
            assertEquals("List size 0", 0, findAll.size());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Failed", ex);
        }
    }

    @Test
    public void testCreateOneMethod() throws Exception {
        try {
            userTransaction.begin();
            List<Contact> findAll = crudService.findByNamedQuery(Contact.ALL);
            assertEquals("List size 0", 0, findAll.size());
            Contact c = new Contact();
            c.setFirstname("Peter");
            c.setLastname("Johansson");

            Phone p = new Phone("+46", "08", "123456", PhoneType.Private);
            c.getPhones().add(p);

            crudService.create(c);
            findAll = crudService.findByNamedQuery(Contact.ALL);
            assertEquals("List size 1", 1, findAll.size());
            crudService.delete(c);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            userTransaction.rollback();
        }
    }

    @Test
    public void testNativeQuery() throws Exception {
        String q = "SELECT c.firstname, c.lastname, a.street, a.zip, a.city "
                + " FROM Contact c left outer join Address a on c.id = a.contact_id";
        List<ReportDTO> findByNativeQuery = crudService.findByNativeQuery(q,
                new String[]{"firstname", "lastname", "street", "zip", "city"},
                ReportDTO.class);

    }
}
